#include "Pentagon.h"
//C'Tor
Pentagon::Pentagon(string nam, string col, double polygon) : Shape(nam, col)
{
	this->_polygon = polygon;
}


//Getter
double Pentagon::getPolygon()
{
	return _polygon;
}
//Setter
void Pentagon::setPolygon(double pol)
{
	_polygon = pol;
}
//Print the details of the shape
void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "Area: " << CalArea() << std::endl;;
}
//Calculates area using static function in MathUtils
double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(_polygon);
}