#pragma once


class MathUtils
{
public:
	static double CalPentagonArea(double polygone);
	static double CalHexagonArea(double polygone);
};