#include "Hexagon.h"
#include "MathUtils.h"

//C'Tor
Hexagon::Hexagon(string nam, string col, double polygon) : Shape(nam, col)
{
	this->_polygon = polygon;
}

//Getter
double Hexagon::getPolygon()
{
	return _polygon;
}

//Setter
void Hexagon::setPolygon(double pol)
{
	_polygon = pol;
}

//print the details of the shape
void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "Area: " << CalArea() << std::endl;;
}

//Calculate the area using the static function in MathUtils
double Hexagon::CalArea()
{
	return MathUtils::CalHexagonArea(_polygon);
}