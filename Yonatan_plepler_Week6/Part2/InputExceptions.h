#pragma once
#include <iostream>
using namespace std;
class InputExceptions : public exception
{
public:
	virtual const char* what() const;
};