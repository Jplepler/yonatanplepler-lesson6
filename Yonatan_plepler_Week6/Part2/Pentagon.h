#pragma once
#include "shape.h"
#include "MathUtils.h"
using namespace std;
class Pentagon : public Shape
{
public:
	Pentagon(string nam, string col, double polygon);
	virtual void draw();
	virtual double CalArea();
	double getPolygon();
	void setPolygon(double pol);
	


private:
	double _polygon;	
};