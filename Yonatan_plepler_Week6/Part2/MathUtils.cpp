#include "MathUtils.h"
#include <cmath>
#define QUARTER 0.25
#define TWO 2
#define THREE 3
#define FIVE 5


//Two static functions for calculating area of polygon and hexagon with one polygon
double MathUtils::CalPentagonArea(double polygone)
{
	return (QUARTER * sqrt(FIVE * (FIVE + (TWO * sqrt(FIVE)))) * pow(polygone, TWO));
}

double MathUtils::CalHexagonArea(double polygone)
{
	return (((THREE * sqrt(THREE)) / TWO) * pow(polygone, TWO) );
}