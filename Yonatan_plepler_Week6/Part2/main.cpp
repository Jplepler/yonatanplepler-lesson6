#pragma once
#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputExceptions.h"
#include "Hexagon.h"
#include "Pentagon.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, pol = 0; int height = 0, width = 0;
	Circle circ(nam, col, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pent(nam, col, pol);
	Hexagon hexa(nam, col, pol);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape* ptrpenta = &pent;
	Shape* ptrhexa = &hexa;

	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, t = pentagon" << std::endl;
		shapetype = getchar();
		
		
		

		try
		{
			//Check if more then one character has been entered
			if (std::cin.rdbuf()->in_avail() > 1)
			{
				cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');//Clean cin buffer
				cout << "Warning - Don't try  to build more than one shape at once" << endl;
				throw exception();
			}
			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;

			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;

			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;

			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;


			case 't':
				std::cout << "enter name, color, polygon" << std::endl;
				std::cin >> nam >> col >> pol;
				pent.setColor(col);
				pent.setName(nam);
				pent.setPolygon(pol);
				ptrpenta->draw();
				break;

			case 'h':
				std::cout << "enter name, color, polygon" << std::endl;
				std::cin >> nam >> col >> pol;
				hexa.setColor(col);
				hexa.setName(nam);
				hexa.setPolygon(pol);
				ptrhexa->draw();
				break;

			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			

			if (cin.fail())
			{
				
				//get rid of failure state
				cin.clear();
				// discard 'bad' character(s) 
				cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				throw InputExceptions();
			}
			else
			{
				std::cin.get();
				std::cout << "would you like to add more object press any key if not press x" << std::endl;
				std::cin.get(x);
			}
		}
		catch (shapeException e)
		{			
			printf(e.what());
		}
		catch (InputExceptions e)
		{
			std::cout << e.what();
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}

	system("PAUSE");
	return 0;
	
}