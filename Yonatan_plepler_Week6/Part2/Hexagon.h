#pragma once
#include "shape.h"
using namespace std;
class Hexagon : public Shape
{
public:
	Hexagon(string nam, string col, double polygon);
	virtual void draw();
	virtual double CalArea();
	double getPolygon();
	void setPolygon(double pol);



private:
	double _polygon;
};